package com.cs.test.service.impl;

import com.cs.test.common.ResponseCode;
import com.cs.test.common.ServerResponse;
import com.cs.test.config.RabbitConfig;
import com.cs.test.mapper.MsgLogMapper;
import com.cs.test.mq.util.MessageHelper;
import com.cs.test.pojo.Mail;
import com.cs.test.pojo.MsgLog;
import com.cs.test.service.TestService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 关于发送消息有
 *
 * @author james
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MsgLogMapper msgLogMapper;

    @Override
    public ServerResponse send(Mail mail) {
        String msgId = RandomStringUtils.randomNumeric(32);
        mail.setMsgId(msgId);
        CorrelationData correlationData = new CorrelationData(msgId);
        MsgLog msgLog = new MsgLog(msgId, mail, RabbitConfig.MAIL_EXCHANGE_NAME, RabbitConfig.MAIL_ROUTING_KEY_NAME);
        // 消息入库记录
        msgLogMapper.insert(msgLog);
        /**
         * 使用路由工作模式发送消息. 至于消息是否发送到Exchange, Exchange是否发送到queue然有人帮我们做了记录
         * {@link com.cs.test.config.RabbitConfig#rabbitTemplate()}
         */
        rabbitTemplate.convertAndSend(RabbitConfig.MAIL_EXCHANGE_NAME,
                RabbitConfig.MAIL_ROUTING_KEY_NAME, MessageHelper.objToMsg(mail),
                correlationData);
        return ServerResponse.success(ResponseCode.MAIL_SEND_SUCCESS.getMsg());
    }


}
