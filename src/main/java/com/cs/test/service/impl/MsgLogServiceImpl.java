package com.cs.test.service.impl;

import com.cs.test.mapper.MsgLogMapper;
import com.cs.test.pojo.MsgLog;
import com.cs.test.service.MsgLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.Date;
import java.util.List;

/**
 * 关于投递到mq的消息日志相关
 */
@Service
public class MsgLogServiceImpl implements MsgLogService {

    @Autowired
    private MsgLogMapper logMapper;

    @Override
    public void insert(MsgLog msgLog) {
        logMapper.insert(msgLog);
    }

    @Override
    public void updateStatus(@NonNull String msgId, Integer status) {
        MsgLog msgLog = new MsgLog();
        msgLog.setMsgId(msgId);
        msgLog.setStatus(status);
        logMapper.updateStatus(msgLog);
    }

    @Override
    public MsgLog selectByMsgId(String msgId) {
        return logMapper.selectByPrimaryKey(msgId);
    }

    @Override
    public List<MsgLog> selectTimeoutMsg() {
        return logMapper.selectTimeoutMsg();
    }

    @Override
    public void updateTryCount(String msgId, Date tryTime) {
        MsgLog log = new MsgLog();
        log.setMsgId(msgId);
        log.setNextTryTime(tryTime);
        logMapper.updateTryCount(log);
    }

    /**
     * 计算π
     * @param n
     * @return
     */
    public static double MonteCarloPI(int n){
        double PI;
        double x,y;
        int  i,sum=0;
        for( i=0;i<n;i++){
            x=Math.random();
            y=Math.random();
            if((x*x+y*y)<=1){
                sum++;
            }
        }
        PI=4.0*sum/n;
        return PI;
    }
    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        System.out.println("PI:"+MonteCarloPI(100000000));
        long l1 = System.currentTimeMillis();
        System.out.println(l1-l);
    }
}
