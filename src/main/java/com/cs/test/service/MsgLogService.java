package com.cs.test.service;

import com.cs.test.pojo.MsgLog;

import java.util.Date;
import java.util.List;

public interface MsgLogService {
    void insert(MsgLog msgLog);

    void updateStatus(String msgId, Integer status);

    MsgLog selectByMsgId(String msgId);

    List<MsgLog> selectTimeoutMsg();

    void updateTryCount(String msgId, Date tryTime);
}