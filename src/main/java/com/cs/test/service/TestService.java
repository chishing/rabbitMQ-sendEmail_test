package com.cs.test.service;

import com.cs.test.common.ServerResponse;
import com.cs.test.pojo.Mail;

public interface TestService {

//    ServerResponse testIdempotence();

//    ServerResponse accessLimit();

    ServerResponse send(Mail mail);
}
