package com.cs.test.controller;

import com.cs.test.common.ServerResponse;
import com.cs.test.pojo.Mail;
import com.cs.test.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author james
 */
@RestController
@Slf4j
@RequestMapping("/test")
public class TestSendEmailController {

    @Autowired
    private TestService testService;

    @PostMapping("/send")
    public ServerResponse sendEmail(@Validated Mail mail,Errors errors){
        if (errors.hasErrors()){
            String msg = errors.getFieldError().getDefaultMessage();
            return ServerResponse.error(msg);
        }
        System.out.println(mail);
        testService.send(mail);
        return ServerResponse.success("ad");
    }
}
