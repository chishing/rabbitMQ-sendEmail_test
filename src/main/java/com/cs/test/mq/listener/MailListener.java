package com.cs.test.mq.listener;

import com.cs.test.config.RabbitConfig;
import com.cs.test.mq.BaseConsumer;
import com.cs.test.mq.BaseConsumerProxy;
import com.cs.test.mq.consumer.MailConsumer;
import com.cs.test.service.MsgLogService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MailListener {

    @Autowired
    private MailConsumer mailConsumer;
    @Autowired
    private MsgLogService msgLogService;


    @RabbitListener(queues = RabbitConfig.MAIL_QUEUE_NAME)
    public void mailConsume(Message message, Channel channel) throws IOException {
        // 我擦, 这里怎么都想不懂是怎么做到的...
        BaseConsumerProxy baseConsumerProxy =
                new BaseConsumerProxy(mailConsumer,msgLogService);
        BaseConsumer proxy = (BaseConsumer)baseConsumerProxy.getProxy();
        if (null != proxy){
            proxy.consume(message,channel);
        }
    }
}
