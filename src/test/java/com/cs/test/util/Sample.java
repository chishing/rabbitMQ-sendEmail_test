package com.cs.test.util;

import com.baidu.aip.face.AipFace;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 复制来自 https://ai.baidu.com/ai-doc/FACE/8k37c1rqz
 */
public class Sample {
    //设置APPID/AK/SK

    public static final String APP_ID = "19199332";
    public static final String API_KEY = "PqTD3wS3EDXiqhk5yeReHbS7";
    public static final String SECRET_KEY = "E6g1mcvq6HqCg30h1FxN2R9ncow5uhyp";
    public static final String BASE_URL = "https://aip.baidubce.com/rest/2.0/face/v3/detect";

    public static void main(String[] args) {
        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
        // client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
        // client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<>();
        // options.put("face_field", "age");
        options.put("max_face_num", "10");
        // 人脸的类型 LIVE表示生活照：通常为手机、相机拍摄的人像图片、或从网络获取的人像图片等IDCARD表示身份证芯片照：二代身份证内置芯片中的人像照片 WATERMARK表示带水印证件照：一般为带水印的小图，如公安网小图 CERT表示证件照片：如拍摄的身份证、工卡、护照、学生证等证件图片 默认LIVE
        options.put("face_type", "LIVE");
        //  LOW:较低的活体要求(高通过率 低攻击拒绝率)
        options.put("liveness_control", "LOW");

        // 调用接口
        // String image = "取决于image_type参数，传入BASE64字符串或URL字符串或FACE_TOKEN字符串";
        // String imageType = "BASE64";

        // String image = "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2915593299,2949637172&fm=11&gp=0.jpg"; // 3个
        // String image = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1585737089798&di=f0811bc058e1ebd3dbba3b5c45bdb6bf&imgtype=0&src=http%3A%2F%2Fdingyue.ws.126.net%2F2020%2F0216%2Feb659e48j00q5sb1o0087c000qo00j6m.jpg"; // 10个
        String image = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1585737089798&di=b9412d0a8847dbc091c231a2435c387c&imgtype=0&src=http%3A%2F%2Fdingyue.ws.126.net%2FieokqjzPFi2GaLCUO5LGi2sWqQMf46QykOSz8mVl83znV1583576758133compressflag.jpeg"; // 2个
        String imageType = "URL";

        // 人脸检测
        JSONObject res = client.detect(image, imageType, options);
        try {
            System.out.println(res.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
