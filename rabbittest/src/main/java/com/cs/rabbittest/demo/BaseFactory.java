package com.cs.rabbittest.demo;

import com.rabbitmq.client.ConnectionFactory;

public class BaseFactory {

    public static ConnectionFactory getConnection(){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        factory.setUsername("guest");
        factory.setPassword("guest");
        // rabbitMQ默认虚拟机名称"/", 虚拟机相当于一个独立的mq服务器
        factory.setVirtualHost("/");
        return factory;
    }
}
