package com.cs.rabbittest.demo.routing;

import com.cs.rabbittest.demo.BaseFactory;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 路由模式
 *
 * @author james
 */
public class Producer03_routing {
    // 队列名称
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email";
    // sms队列名称
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms";
    // 交换机名称
    private static final String EXCHANGE_ROUTING_INFORM = "exchange_routing_inform";

    // 发送次数
    private static final int COUNT = 10;


    public static void main(String[] args) {
        ConnectionFactory factory = BaseFactory.getConnection();
        Connection connection = null;
        Channel channel = null;
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            /**
             * 声明交换机
             * @param1 交换机名称
             * @param2 交换机类型 fanout,topic,direct,headers
             */
            channel.exchangeDeclare(EXCHANGE_ROUTING_INFORM, BuiltinExchangeType.DIRECT);
            // 声明email队列
            /**
             * @param1 队列名称
             * @param2 是否持久化
             * @param3 是否独占此队列
             * @param4 队列不用是否删除
             * @param5 参数
             */
            channel.queueDeclare(QUEUE_INFORM_EMAIL, true, false, false, null);
            // 声明SMS队列
            channel.queueDeclare(QUEUE_INFORM_SMS, true, false, false, null);
            /**
             * 队列和交换机绑定
             * @param1 队列名称
             * @param2 交换机名称
             * @param3 路由key
             */
            channel.queueBind(QUEUE_INFORM_EMAIL, EXCHANGE_ROUTING_INFORM, QUEUE_INFORM_EMAIL);
            channel.queueBind(QUEUE_INFORM_SMS, EXCHANGE_ROUTING_INFORM, QUEUE_INFORM_SMS);
            // 开启确认模式
            channel.confirmSelect();
            // 发送邮件信息
            for (int i = 0; i < COUNT; i++) {
                String message = String.format("email inform to user %d", i);
                /**
                 * @param1 交换机名称
                 * @param2 routingKey(路由key), 根据key名称将消息转发到具体的队列,这里填写队列名称表示消息发送到此队列
                 * @param3 消息属性
                 * @param4 消息内容
                 */
                channel.basicPublish(EXCHANGE_ROUTING_INFORM, QUEUE_INFORM_EMAIL, MessageProperties.MINIMAL_BASIC, message.getBytes("utf-8"));
            }
            // 等待回复，如果回复true
            if (channel.waitForConfirms()) {
                System.out.printf("email 批量发送消息成功\n");
            } else {
                System.out.println("email 批量发送失败");
            }
            // 发送短信消息
            for (int i = 0; i < COUNT; i++) {
                String message = String.format("email inform to user %d", i);
                channel.basicPublish(EXCHANGE_ROUTING_INFORM, QUEUE_INFORM_SMS, MessageProperties.MINIMAL_BASIC, message.getBytes("utf-8"));
            }
            // 等待回复，如果回复true
            if (channel.waitForConfirms()) {
                System.out.printf("SMS 批量发送消息成功\n");
            } else {
                System.out.println("SMS 批量发送失败");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
