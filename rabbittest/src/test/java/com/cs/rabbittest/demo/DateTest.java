package com.cs.rabbittest.demo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class DateTest {

    public static void main(String[] args) {
        new DateTest().t2();

    }

    void t1() {
        // 当前时间
        Calendar date = Calendar.getInstance();
        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONDAY);
        int dayOfWeek = date.get(Calendar.DAY_OF_MONTH);
        System.out.println(year);
        System.out.println(month);
        System.out.println(dayOfWeek);
    }

    void t2() {
        // 当前日期
        LocalDateTime localDateTime = LocalDateTime.now();
        // 当前年份
        int year = localDateTime.getYear();
        // 当前月份
        int value = localDateTime.getMonth().getValue();
        // 当前日
        int dayOfMonth = localDateTime.getDayOfMonth();
        System.out.println(year);
        System.out.println(value);
        System.out.println(dayOfMonth);
        // 格式化目标日期
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate parse = LocalDate.parse("2020-10-10", dateTimeFormatter);
        int targetYear = parse.getYear();
        int targetValue = parse.getMonth().getValue();
        int targetDayOfMonth = parse.getDayOfMonth();
        System.out.println(targetYear);
        System.out.println(targetValue);
        System.out.println(targetDayOfMonth);
    }
}
